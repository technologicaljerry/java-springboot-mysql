package com.example.usercrud.service;

import com.example.usercrud.entity.User;
import com.example.usercrud.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserServices {

    private UserRepository userRepository;
    public User saveUser(User user){
      return userRepository.save(user);
    }
}
